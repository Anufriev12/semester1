/**
 * Created by FouTER on 04.09.2017.
 */
public class testClass {
    public static void main(String[] args) {
        WaterResourcesList a = new WaterResourcesList();
        River Irtish = new River("Иртыш", 1643000, 4248, 3000);
        StillWater Baikal = new StillWater("Байкал", 31722, typeOfStillwater.LAKE, 23615, 1642);
        StillWater caspianSea = new StillWater("Каспийское", 371000, typeOfStillwater.SEA, 78200, 1025);
        a.addResourse(Irtish);
        a.addResourse(caspianSea);
        a.showAll();
        a.clearList();
        a.addResourse(Baikal);
        a.showAll();
    }
}
