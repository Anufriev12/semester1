public class River extends WaterObject{
    private int length; //km
    private int averageRate; //m^3


    @Override
    public String toString() {
        return ("Река " + super.toString() + "\nПротяженность: " + length + " километров \nРасход воды: "
                + averageRate + " кубических метров в секунду\n");
    }

    public River(String name, int basinSize, int length, int averageRate) {
        super(name, basinSize);
        this.length = length;
        this.averageRate = averageRate;

    }

    public int getLength() {
        return length;
    }

    public int getAverageRate() {
        return averageRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        River river = (River) o;

        if (length != river.length) return false;
        return averageRate == river.averageRate;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + length;
        result = 31 * result + averageRate;
        return result;
    }
}
