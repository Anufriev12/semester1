public class StillWater extends WaterObject{
    private typeOfStillwater type;
    private int volume; //km^2
    private int depth; //m

    public StillWater(String name, int basinSize, typeOfStillwater type, int volume, int depth) {
        super(name, basinSize);
        this.type = type;
        this.volume = volume;
        this.depth = depth;
    }



    @Override
    public String toString() {
        return (type.getTypeString() + ' ' + super.toString() + "\nОбьем: " + volume + " кубических метров \nГлубина: "
                + depth + " метров\n");
    }

    public typeOfStillwater getType() {
        return type;
    }

    public int getVolume() {
        return volume;
    }

    public int getDepth() {
        return depth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        StillWater that = (StillWater) o;

        if (volume != that.volume) return false;
        if (depth != that.depth) return false;
        return type == that.type;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + volume;
        result = 31 * result + depth;
        return result;
    }
}
