public class WaterObject{
    public String name;
    public int basinSize; //km^2

    public WaterObject(String name, int basinSize) {
        this.name = name;
        this.basinSize = basinSize;
    }

    public String getName() {
        return name;
    }

    public int getBasinSize() {
        return basinSize;
    }

    @Override
    public String toString() {
        return (name + "\nПлощадь бассейна: " + basinSize + " квадратных километров");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WaterObject that = (WaterObject) o;

        if (basinSize != that.basinSize) return false;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + basinSize;
        return result;
    }
}
