import java.util.LinkedList;
import java.util.List;

/**
 * Created by FouTER on 04.09.2017.
 */

enum typeOfStillwater{
    LAKE("Озеро"), OCEAN("Океан"), SEA("Море"), PIT("Котлован");

    private final String label;

    private typeOfStillwater(String label) {
        this.label = label;
    }
    public String getTypeString() {
        return this.label;
    }
}

public class WaterResourcesList {
    private List<WaterObject> mainList = null;

    public WaterResourcesList() {
        mainList = new LinkedList<WaterObject>();
    }

    public void addResourse(WaterObject a)
    {
        mainList.add(a);
    }

    public void clearList()
    {
        mainList.clear();
    }

    public boolean isEmpty()
    {
        return mainList.isEmpty();
    }

    public boolean delResourse(WaterObject a)
    {
        boolean contains = mainList.contains(a);
        if(!contains) return false;
        mainList.remove(a);
        return true;
    }

    public boolean delResourseByName(String a)
    {
        for(WaterObject b : mainList)
        {
            if(b.getName() == a){
                mainList.remove(b);
                return true;
            }

        }
        return false;
    }

    public void showAll()
    {
        for(WaterObject a : mainList)
        {
            System.out.println(a.toString());
        }
    }
}









