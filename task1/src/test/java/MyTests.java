import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 * Created by FouTER on 04.09.2017.
 */
public class MyTests {

    @Test
    public void addingTest()
    {
    WaterResourcesList a = new WaterResourcesList();
    StillWater Baikal = new StillWater("Байкал", 31722, typeOfStillwater.LAKE, 23615, 1642);
    a.addResourse(Baikal);
    assertTrue(!a.isEmpty());
    }

    @Test
    public void isEmptyTest()
    {
        WaterResourcesList a= new WaterResourcesList();
        assertTrue(a.isEmpty());
    }

    @Test
    public void removeByNameTest()
    {
        WaterResourcesList a = new WaterResourcesList();
        StillWater Baikal = new StillWater("Байкал", 31722, typeOfStillwater.LAKE, 23615, 1642);
        a.addResourse(Baikal);
        a.delResourseByName("Байкал");
        assertTrue(a.isEmpty());
    }

    @Test
    public void removeByNameObjectTest()
    {
        WaterResourcesList a = new WaterResourcesList();
        StillWater Baikal = new StillWater("Байкал", 31722, typeOfStillwater.LAKE, 23615, 1642);
        a.addResourse(Baikal);
        a.delResourse(Baikal);
        assertTrue(a.isEmpty());
    }


}
