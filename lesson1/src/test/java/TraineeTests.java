import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by FouTER on 11.09.2017.
 */
public class TraineeTests {

    @Test
    public void testCorrectTrainees() throws TraineeException {
        Trainee a = new Trainee("Сергей", "Виноградов", 5);
        Trainee b = new Trainee("Дмитрий", "Гриценко", 4);
        assertTrue(true);
    }

    @Test
    public void testIncorrectTrainees() throws TraineeException {
        try {
            Trainee a = new Trainee("Сергей", "Виноградов", 6);
            assertTrue(false);
        } catch (TraineeException a) {
            assertTrue(true);
        }
    }

    @Test
    public void testIncorrectSetter() throws TraineeException {
        Trainee a = new Trainee("Сергей", "Виноградов", 5);

        try {
            a.setGrade(0);
            assertTrue(false);//Если мы здесь, то исключение не проброшено
        } catch (TraineeException b) {
            assertTrue(true);
        }

    }
}
