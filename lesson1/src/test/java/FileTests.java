import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by FouTER on 11.09.2017.
 */
public class FileTests {

    @Test
    public void createFileTest() {
        boolean correct;
        File dir = new File("testDir");
        File f = new File("testDir/file.txt");

        correct = dir.mkdir();
        try {
            correct = correct && f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
        assert true;
    }

    @Test
    public void checkExistenceTest() {
        File f = new File("testDir/file.dat");
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
        assertTrue(f.exists());
    }

    @Test
    public void removeFileTest() {
        File f = new File("testDir/fileToDel.txt");
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
        f.delete();
        assertFalse(f.exists());
    }

    @Test
    public void renameFileTest() {
        File f = new File("testDir/toRename.dat");
        File r = new File("testDir/renamed.dat");
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
        f.renameTo(r);
        assertTrue(r.exists());
    }

    @Test
    public void checkTypeTest() {
        File f = new File("testDir/file.dat");
        File dir = new File("testDir");
        if(!dir.exists()) dir.mkdir();
        if (!f.exists()) try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
        assertTrue(f.isFile() && dir.isDirectory());
    }

    @Test
    public void getAbsolutePathTest() throws Exception {
        File f = new File("testDir/file.dat");
        if (!f.exists()) try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
        System.out.println(f.getAbsolutePath());
    }

    @Test
    public void getFullListOfFilesTest() {

    }

    @Test
    public void getListOfFilesByMaskTest() {

    }
}
