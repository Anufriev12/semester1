/**
 * Created by FouTER on 11.09.2017.
 */
class TraineeException extends Exception{

    private TraineeErrorCodes errorCode = null;

    public TraineeException(TraineeErrorCodes a) {
    errorCode = a;
    }

    String getErrorString(){return errorCode.getErrorString();}

}

enum TraineeErrorCodes{
    INCORRECT_NAME("Некорректное имя"), INCORRECT_SURNAME("Некорректная фамилия"), INCORRECT_GRADE("Некорректная оценка");
    private String errorString;

    TraineeErrorCodes(String a)
    {
        errorString = a;
    }

    public String getErrorString() {
        return errorString;
    }
}

public class Trainee {
    private String name = null;
    private String surname = null;
    private int grade = 0;

    public Trainee(String name, String surname, int grade) throws TraineeException {
        if(name == null || name.equals("")) throw new TraineeException(TraineeErrorCodes.INCORRECT_NAME);
        if(surname == null || surname.equals("")) throw new TraineeException(TraineeErrorCodes.INCORRECT_SURNAME);
        if(grade < 1 || grade > 5) throw new TraineeException(TraineeErrorCodes.INCORRECT_GRADE);
        this.name = name;
        this.surname = surname;
        this.grade = grade;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws TraineeException {
        if(name == null || name.equals("")) throw new TraineeException(TraineeErrorCodes.INCORRECT_NAME);
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) throws TraineeException {
        if(surname == null || surname.equals("")) throw new TraineeException(TraineeErrorCodes.INCORRECT_SURNAME);
        this.surname = surname;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) throws TraineeException {
        if(grade < 1 || grade > 5) throw new TraineeException(TraineeErrorCodes.INCORRECT_GRADE);
        this.grade = grade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Trainee trainee = (Trainee) o;

        if (grade != trainee.grade) return false;
        if (!name.equals(trainee.name)) return false;
        return surname.equals(trainee.surname);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + grade;
        return result;
    }
}
